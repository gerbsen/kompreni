# Kompreni
Dieses kleine Tool soll eine statistische Analyse der Kommentare auf LVZ.de durchführen.
Im Grunde definiert man eine Startseite (LVZ.de) und hangelt sich dann an Hand der Links innerhalb der Startseite durch die ganze Domain. 
Da nicht alle Seiten für diese Analyse relevant sind können wir das vorher auf http://www.lvz.de/Leipzig/\*, http://www.lvz.de/Region/\* oder http://www.lvz.de/Nachrichten/\* beschränken.
Bei den Artikeln selbst muss man ein bisschen mit jQuery bzw. hier Cheerio herumwursteln um strukturierte Informationen zu erhalten.
Dankenswerterweise gibt es für die Kommentare eine API die passend zum Artikel gleich alles in JSON liefert.

## Install
Als ersten müssen wir eine PostgreSQL Datenbank anlegen und einen User zuweisen:

```
$ psql -U postgres
$ CREATE DATABASE kompreni;
$ CREATE ROLE kompreni LOGIN;
$ GRANT ALL PRIVILEGES ON DATABASE kompreni to kompreni; 
```  

# Schema
```
{
    "publishedDate": "TODO",
    "crawledDate": "2018-09-03T19:26:22.001Z",
    "articleId": "23163607",
    "title": "65.000 Menschen setzten bei #wirsindmehr in Chemnitz ein Zeichen",
    "subTitle": "Zehntausende Besucher sind nach Chemnitz gekommen, um Flagge gegen Rechts zu zeigen. Nach einer Schweigeminute für den getöteten 35-Jährigen begann Trettmann, die anderen Acts legten musikalisch nach. LVZ.de berichtet im Liveticker.",
    "image": "http://www.lvz.de/Region/var/storage/images/lvz/region/mitteldeutschland/liveticker-konzert-gegen-rechts-in-chemnitz/702536100-17-ger-DE/Die-Toten-Hosen-als-letzte-Band-bei-wirsindmehr-auf-der-Buehne_big_teaser_article.jpg",
    "body": "<html>SOME HTML</html>",
    "comments": [
        {
            "subject": "Gegendemonstrationen werden nicht zugelassen",
            "content": "Sicherlich aus der Erkenntnis, dass am Samstag eine angemeldete Demonstration durch eine Gegendemonstration verhindert wurde, will man jetzt wohl die im Grundgesetz verankerten demokratischen Regeln durchsetzen. Also wieder mal \"Wir schaffen das\"",
            "updated": "2018-09-03T14:38:41+02:00",
            "from": "controll",
            "is_moderator": false
        },
        ...
    ]
}
```

# Todos
- [ ] Kommt man an Kommentare bevor sie gelöscht wurden?
- [ ] Speichern der Artikel in einer Datenbank, um einfacher Abfragen durchführen zu können.
- [ ] Wiederholtes des Crawls und Updates der Kommentare
- [ ] NodeJS Template Lösung raussuchen, damit wir direkt eine Bootstrap Seite nach dem Crawl mit den Statistiken erzeugen können 
- [ ] Seiten müssen irgendwie getaggt werden
- [ ] Seiten aus dem Archiv werden nicht berücksichtigt

# Mögliche Statistiken die zu erstellen sind
- [ ] Kommentare pro Seite pro Tag/Monat/Jahr
- [ ] Kommentare pro Tag
- [ ] häufigste Worte
- [ ] häufigste User

(mental notes)
```
rp({ 'uri' : "http://www.lvz.de/comments/" + articleId, 'json' : true })
                .then(function (commentsDto) {
                    
                    page.comments = commentsDto.comments;
                    
                    if ( page.title !== '' ) {

                        fs.writeFile("./pages/" + articleId + ".json", JSON.stringify(page, null, 4), function(err) {
                            if ( err ) {
                                return console.log(err);
                            }
                        });    
                    }
                })
                .catch(function (err) {

                    console.log(err);
                });
```
