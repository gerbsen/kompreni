import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiDataUtils } from 'ng-jhipster';

import { IArticle } from 'app/shared/model/article.model';
import { ArticleService } from './article.service';

@Component({
    selector: 'jhi-article-update',
    templateUrl: './article-update.component.html'
})
export class ArticleUpdateComponent implements OnInit {
    private _article: IArticle;
    isSaving: boolean;
    publishedDate: string;
    crawledDate: string;
    nextCrawlDate: string;

    constructor(private dataUtils: JhiDataUtils, private articleService: ArticleService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ article }) => {
            this.article = article;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.article.publishedDate = moment(this.publishedDate, DATE_TIME_FORMAT);
        this.article.crawledDate = moment(this.crawledDate, DATE_TIME_FORMAT);
        this.article.nextCrawlDate = moment(this.nextCrawlDate, DATE_TIME_FORMAT);
        if (this.article.id !== undefined) {
            this.subscribeToSaveResponse(this.articleService.update(this.article));
        } else {
            this.subscribeToSaveResponse(this.articleService.create(this.article));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IArticle>>) {
        result.subscribe((res: HttpResponse<IArticle>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get article() {
        return this._article;
    }

    set article(article: IArticle) {
        this._article = article;
        this.publishedDate = moment(article.publishedDate).format(DATE_TIME_FORMAT);
        this.crawledDate = moment(article.crawledDate).format(DATE_TIME_FORMAT);
        this.nextCrawlDate = moment(article.nextCrawlDate).format(DATE_TIME_FORMAT);
    }
}
