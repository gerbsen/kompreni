import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IArticle } from 'app/shared/model/article.model';

type EntityResponseType = HttpResponse<IArticle>;
type EntityArrayResponseType = HttpResponse<IArticle[]>;

@Injectable({ providedIn: 'root' })
export class ArticleService {
    private resourceUrl = SERVER_API_URL + 'api/articles';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/articles';

    constructor(private http: HttpClient) {}

    create(article: IArticle): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(article);
        return this.http
            .post<IArticle>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(article: IArticle): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(article);
        return this.http
            .put<IArticle>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IArticle>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IArticle[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IArticle[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    private convertDateFromClient(article: IArticle): IArticle {
        const copy: IArticle = Object.assign({}, article, {
            publishedDate: article.publishedDate != null && article.publishedDate.isValid() ? article.publishedDate.toJSON() : null,
            crawledDate: article.crawledDate != null && article.crawledDate.isValid() ? article.crawledDate.toJSON() : null,
            nextCrawlDate: article.nextCrawlDate != null && article.nextCrawlDate.isValid() ? article.nextCrawlDate.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.publishedDate = res.body.publishedDate != null ? moment(res.body.publishedDate) : null;
        res.body.crawledDate = res.body.crawledDate != null ? moment(res.body.crawledDate) : null;
        res.body.nextCrawlDate = res.body.nextCrawlDate != null ? moment(res.body.nextCrawlDate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((article: IArticle) => {
            article.publishedDate = article.publishedDate != null ? moment(article.publishedDate) : null;
            article.crawledDate = article.crawledDate != null ? moment(article.crawledDate) : null;
            article.nextCrawlDate = article.nextCrawlDate != null ? moment(article.nextCrawlDate) : null;
        });
        return res;
    }
}
