import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils, JhiAlertService } from 'ng-jhipster';

import { IOccurrence } from 'app/shared/model/occurrence.model';


@Component({
    selector: 'jhi-occurrence-view',
    templateUrl: './comment-word-frequency.component.html'
})
export class OccurrenceViewComponent implements OnInit {

    @Input()
    occurrences: IOccurrence[];

    constructor(
        private dataUtils: JhiDataUtils
    ) {}

    ngOnInit() {}

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }
}
