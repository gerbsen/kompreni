import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { createRequestOption } from 'app/shared';
import { SERVER_API_URL } from 'app/app.constants';
import { IOccurrence } from 'app/shared/model/occurrence.model';

type EntityArrayResponseType = HttpResponse<IOccurrence[]>;

@Injectable({ providedIn: 'root' })
export class OccurrenceService {
    private resourceUrl = SERVER_API_URL + 'api/comments';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/comments';

    constructor(private http: HttpClient) {}

    getCommentsWordFrequency(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IOccurrence[]>(`${this.resourceUrl}/word/frequency`,
            { params: options, observe: 'response' });
    }

    getCommentAuthorFrequency(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IOccurrence[]>(`${this.resourceUrl}/author/frequency`,
            { params: options, observe: 'response' });
    }
}
