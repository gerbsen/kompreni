import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { KompreniCommentModule } from './comment/comment.module';
import { KompreniArticleModule } from './article/article.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        KompreniCommentModule,
        KompreniArticleModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class KompreniEntityModule {}
