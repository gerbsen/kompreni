import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { KompreniSharedModule } from 'app/shared';
import { HOME_ROUTE, HomeComponent } from './';
import { OccurrenceViewComponent } from 'app/entities/occurrence/comment-word-frequency/comment-word-frequency.component';

@NgModule({
    imports: [KompreniSharedModule, RouterModule.forChild([HOME_ROUTE])],
    declarations: [HomeComponent, OccurrenceViewComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class KompreniHomeModule {}
