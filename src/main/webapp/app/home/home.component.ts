import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { LoginModalService, Principal, Account } from 'app/core';
import { OccurrenceService } from 'app/entities/occurrence/occurrence.service';
import { IOccurrence } from 'app/shared/model/occurrence.model';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit {
    
    account: Account;
    modalRef: NgbModalRef;
    wordOccurrences: IOccurrence[];
    authorOccurrences: IOccurrence[];

    constructor(private principal: Principal, private loginModalService: LoginModalService,
        private eventManager: JhiEventManager, private occurrenceService: OccurrenceService,
        private jhiAlertService: JhiAlertService) {}

    ngOnInit() {
        this.principal.identity().then(account => {
            this.account = account;
        });
        this.registerAuthenticationSuccess();

        this.occurrenceService.getCommentsWordFrequency({
            'order': 'desc',
            'limit' : 25
        }).subscribe(
            (res: HttpResponse<IOccurrence[]>) => {
                this.wordOccurrences = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );

        this.occurrenceService.getCommentAuthorFrequency({
            'order': 'desc',
            'limit' : 25
        }).subscribe(
            (res: HttpResponse<IOccurrence[]>) => {
                this.authorOccurrences = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', message => {
            this.principal.identity().then(account => {
                this.account = account;
            });
        });
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
