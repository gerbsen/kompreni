import { Moment } from 'moment';
import { IComment } from 'app/shared/model//comment.model';

export interface IArticle {
    id?: number;
    valid?: boolean;
    publishedDate?: Moment;
    crawledDate?: Moment;
    articleId?: string;
    title?: string;
    subTitle?: string;
    imageUrl?: string;
    content?: any;
    contentHtml?: any;
    newspaper?: string;
    url?: string;
    nextCrawlDate?: Moment;
    crawlCount?: number;
    comments?: IComment[];
}

export class Article implements IArticle {
    constructor(
        public id?: number,
        public valid?: boolean,
        public publishedDate?: Moment,
        public crawledDate?: Moment,
        public articleId?: string,
        public title?: string,
        public subTitle?: string,
        public imageUrl?: string,
        public content?: any,
        public contentHtml?: any,
        public newspaper?: string,
        public url?: string,
        public nextCrawlDate?: Moment,
        public crawlCount?: number,
        public comments?: IComment[]
    ) {
        this.valid = this.valid || false;
    }
}
