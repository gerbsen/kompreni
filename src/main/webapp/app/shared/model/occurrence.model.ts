export interface IOccurrence {
    occurrence?: number;
    token?: string;
}

export class Occurrence implements IOccurrence {
    constructor(
        public occurrence?: number,
        public token?: string,
    ) {
    }
}
