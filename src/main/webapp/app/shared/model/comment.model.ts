import { Moment } from 'moment';
import { IArticle } from 'app/shared/model//article.model';

export interface IComment {
    id?: number;
    updated?: Moment;
    created?: Moment;
    author?: string;
    moderator?: boolean;
    deleted?: boolean;
    subject?: any;
    content?: any;
    originalSubject?: any;
    originalContent?: any;
    position?: number;
    article?: IArticle;
}

export class Comment implements IComment {
    constructor(
        public id?: number,
        public updated?: Moment,
        public created?: Moment,
        public author?: string,
        public moderator?: boolean,
        public deleted?: boolean,
        public subject?: any,
        public content?: any,
        public originalSubject?: any,
        public originalContent?: any,
        public position?: number,
        public article?: IArticle
    ) {
        this.moderator = this.moderator || false;
        this.deleted = this.deleted || false;
    }
}
