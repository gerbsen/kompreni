package de.codefor.le.kompreni.web.rest;

import com.codahale.metrics.annotation.Timed;
import de.codefor.le.kompreni.domain.Comment;
import de.codefor.le.kompreni.nlp.CommentUtil;
import de.codefor.le.kompreni.domain.Occurrence;
import de.codefor.le.kompreni.repository.CommentRepository;
import de.codefor.le.kompreni.repository.search.CommentSearchRepository;
import de.codefor.le.kompreni.web.rest.errors.BadRequestAlertException;
import de.codefor.le.kompreni.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Comment.
 */
@RestController
@RequestMapping("/api")
public class CommentResource {

    private final Logger log = LoggerFactory.getLogger(CommentResource.class);

    private static final String ENTITY_NAME = "comment";

    private final CommentUtil commentUtil;

    private final CommentRepository commentRepository;

    private final CommentSearchRepository commentSearchRepository;

    public CommentResource(CommentRepository commentRepository, CommentSearchRepository commentSearchRepository, CommentUtil commentUtil) {
        this.commentRepository = commentRepository;
        this.commentSearchRepository = commentSearchRepository;
        this.commentUtil = commentUtil;
    }

    /**
     * POST  /comments : Create a new comment.
     *
     * @param comment the comment to create
     * @return the ResponseEntity with status 201 (Created) and with body the new comment, or with status 400 (Bad Request) if the comment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/comments")
    @Timed
    public ResponseEntity<Comment> createComment(@RequestBody Comment comment) throws URISyntaxException {
        log.debug("REST request to save Comment : {}", comment);
        if (comment.getId() != null) {
            throw new BadRequestAlertException("A new comment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Comment result = commentRepository.save(comment);
        commentSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/comments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /comments : Updates an existing comment.
     *
     * @param comment the comment to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated comment,
     * or with status 400 (Bad Request) if the comment is not valid,
     * or with status 500 (Internal Server Error) if the comment couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/comments")
    @Timed
    public ResponseEntity<Comment> updateComment(@RequestBody Comment comment) throws URISyntaxException {
        log.debug("REST request to update Comment : {}", comment);
        if (comment.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Comment result = commentRepository.save(comment);
        commentSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, comment.getId().toString()))
            .body(result);
    }

    /**
     * GET  /comments : get all the comments.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of comments in body
     */
    @GetMapping("/comments")
    @Timed
    public List<Comment> getAllComments() {
        log.debug("REST request to get all Comments");
        return commentRepository.findAll();
    }

    @GetMapping("/comments/word/frequency")
    @Timed
    public List<Occurrence> getAllCommentsWordFrequency(@RequestParam("order") String order,
                                                        @RequestParam("limit") Integer limit) {
        log.debug("REST request to get the frequency of all words in all comments");
        // TODO cache wrong vales for order (only desc/asc) and limit (only postive interges)

        return CommentUtil.getWordFrequencies(commentRepository.findAllByDeleted(false), limit, order);
    }

    @GetMapping("/comments/author/frequency")
    @Timed
    public List<Occurrence> getAuthorByFrequency(@RequestParam("order") String order,
                                                 @RequestParam("limit") Integer limit) {
        log.debug("REST request to get the number of all comments by author");

        // TODO cache wrong vales for order (only desc/asc) and limit (only postive interges)
        return commentUtil.getGroupByAuthorCount(order, limit != null ? limit : 10);
    }

    @GetMapping("/comments/author/cluster")
    @Timed
    public List<Occurrence> getAuthorCluster(@RequestParam("order") String order,
                                                 @RequestParam("limit") Integer limit) {
        log.debug("REST request to get the number of all comments by author");

        // TODO cache wrong vales for order (only desc/asc) and limit (only postive interges)
        return commentUtil.getGroupByAuthorCount(order, limit != null ? limit : 10);
    }

    /**
     * GET  /comments/:id : get the "id" comment.
     *
     * @param id the id of the comment to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the comment, or with status 404 (Not Found)
     */
    @GetMapping("/comments/{id}")
    @Timed
    public ResponseEntity<Comment> getComment(@PathVariable Long id) {
        log.debug("REST request to get Comment : {}", id);
        Optional<Comment> comment = commentRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(comment);
    }

    /**
     * DELETE  /comments/:id : delete the "id" comment.
     *
     * @param id the id of the comment to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/comments/{id}")
    @Timed
    public ResponseEntity<Void> deleteComment(@PathVariable Long id) {
        log.debug("REST request to delete Comment : {}", id);

        commentRepository.deleteById(id);
        commentSearchRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/comments?query=:query : search for the comment corresponding
     * to the query.
     *
     * @param query the query of the comment search
     * @return the result of the search
     */
    @GetMapping("/_search/comments")
    @Timed
    public List<Comment> searchComments(@RequestParam String query) {
        log.debug("REST request to search Comments for query {}", query);
        return StreamSupport
            .stream(commentSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
