/**
 * View Models used by Spring MVC REST controllers.
 */
package de.codefor.le.kompreni.web.rest.vm;
