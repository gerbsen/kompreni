package de.codefor.le.kompreni.domain;

import java.util.Map;

public class Occurrence implements Comparable<Occurrence> {

    private final int occurence;
    private final String token;

    public Occurrence(String token, Long occurence) {
        this.occurence = occurence.intValue();
        this.token = token;
    }

    public Occurrence(String token, Integer occurence) {
        this.occurence = occurence;
        this.token = token;
    }

    public Occurrence(Map.Entry<String, Integer> entry) {
        this.occurence = entry.getValue();
        this.token = entry.getKey();
    }

    public int getOccurence() {
        return occurence;
    }

    public String getToken() {
        return token;
    }

    @Override
    public int compareTo(Occurrence o) {
        return o.occurence - occurence;
    }
}
