package de.codefor.le.kompreni.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Comment.
 */
@Entity
@Table(name = "comment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "comment")
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "updated")
    private Instant updated;

    @Column(name = "created")
    private Instant created;

    @Column(name = "author")
    private String author;

    @Column(name = "moderator")
    private Boolean moderator;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "subject", columnDefinition="TEXT")
    private String subject;

    @Column(name = "content", columnDefinition="TEXT")
    private String content;

    @Column(name = "original_subject", columnDefinition="TEXT")
    private String originalSubject;

    @Column(name = "original_content", columnDefinition="TEXT")
    private String originalContent;

    @Column(name = "position")
    private Integer position;

    @ManyToOne
    @JsonIgnoreProperties("")
    private Article article;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getUpdated() {
        return updated;
    }

    public Comment updated(Instant updated) {
        this.updated = updated;
        return this;
    }

    public void setUpdated(Instant updated) {
        this.updated = updated;
    }

    public Instant getCreated() {
        return created;
    }

    public Comment created(Instant created) {
        this.created = created;
        return this;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }

    public String getAuthor() {
        return author;
    }

    public Comment author(String author) {
        this.author = author;
        return this;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Boolean isModerator() {
        return moderator;
    }

    public Comment moderator(Boolean moderator) {
        this.moderator = moderator;
        return this;
    }

    public void setModerator(Boolean moderator) {
        this.moderator = moderator;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public Comment deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getSubject() {
        return subject;
    }

    public Comment subject(String subject) {
        this.subject = subject;
        return this;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public Comment content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOriginalSubject() {
        return originalSubject;
    }

    public Comment originalSubject(String originalSubject) {
        this.originalSubject = originalSubject;
        return this;
    }

    public void setOriginalSubject(String originalSubject) {
        this.originalSubject = originalSubject;
    }

    public String getOriginalContent() {
        return originalContent;
    }

    public Comment originalContent(String originalContent) {
        this.originalContent = originalContent;
        return this;
    }

    public void setOriginalContent(String originalContent) {
        this.originalContent = originalContent;
    }

    public Integer getPosition() {
        return position;
    }

    public Comment position(Integer position) {
        this.position = position;
        return this;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Article getArticle() {
        return article;
    }

    public Comment article(Article article) {
        this.article = article;
        return this;
    }

    public void setArticle(Article article) {
        this.article = article;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Comment comment = (Comment) o;
        if (comment.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), comment.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Comment{" +
            "id=" + getId() +
            ", updated='" + getUpdated() + "'" +
            ", created='" + getCreated() + "'" +
            ", author='" + getAuthor() + "'" +
            ", moderator='" + isModerator() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", subject='" + getSubject() + "'" +
            ", content='" + getContent() + "'" +
            ", originalSubject='" + getOriginalSubject() + "'" +
            ", originalContent='" + getOriginalContent() + "'" +
            ", position=" + getPosition() +
            "}";
    }
}
