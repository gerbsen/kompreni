package de.codefor.le.kompreni.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * A Article.
 */
@Entity
@Table(name = "article")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "article")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "valid")
    private Boolean valid;

    @Column(name = "published_date")
    private Instant publishedDate;

    @Column(name = "crawled_date")
    private Instant crawledDate;

    @Column(name = "article_id")
    private String articleId;

    @Column(name = "crawl_count")
    private Integer crawlCount;

    @Size(max = 1024)
    @Column(name = "title", length = 1024)
    private String title;

    @Size(max = 1024)
    @Column(name = "sub_title", length = 1024)
    private String subTitle;

    @Size(max = 1024)
    @Column(name = "image_url", length = 1024)
    private String imageUrl;

    @Column(name = "content", columnDefinition="TEXT")
    private String content;

    @Column(name = "content_html", columnDefinition="TEXT")
    private String contentHtml;

    @Column(name = "newspaper")
    private String newspaper;

    @Size(max = 1024)
    @Column(name = "url", length = 1024)
    private String url;

    @Column(name = "next_crawl_date")
    private ZonedDateTime nextCrawlDate;

    @OneToMany(mappedBy = "article", cascade = CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @OrderBy("index asc")
    private SortedSet<Comment> comments = new TreeSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isValid() {
        return valid;
    }

    public Article valid(Boolean valid) {
        this.valid = valid;
        return this;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public Instant getPublishedDate() {
        return publishedDate;
    }

    public Article publishedDate(Instant publishedDate) {
        this.publishedDate = publishedDate;
        return this;
    }

    public void setPublishedDate(Instant publishedDate) {
        this.publishedDate = publishedDate;
    }

    public Instant getCrawledDate() {
        return crawledDate;
    }

    public Article crawledDate(Instant crawledDate) {
        this.crawledDate = crawledDate;
        return this;
    }

    public void setCrawledDate(Instant crawledDate) {
        this.crawledDate = crawledDate;
    }

    public String getArticleId() {
        return articleId;
    }

    public Article articleId(String articleId) {
        this.articleId = articleId;
        return this;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getTitle() {
        return title;
    }

    public Article title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public Article subTitle(String subTitle) {
        this.subTitle = subTitle;
        return this;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Article imageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getContent() {
        return content;
    }

    public Article content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentHtml() {
        return contentHtml;
    }

    public Article contentHtml(String contentHtml) {
        this.contentHtml = contentHtml;
        return this;
    }

    public void setContentHtml(String contentHtml) {
        this.contentHtml = contentHtml;
    }

    public String getNewspaper() {
        return newspaper;
    }

    public Article newspaper(String newspaper) {
        this.newspaper = newspaper;
        return this;
    }

    public void setNewspaper(String newspaper) {
        this.newspaper = newspaper;
    }

    public String getUrl() {
        return url;
    }

    public Article url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ZonedDateTime getNextCrawlDate() {
        return nextCrawlDate;
    }

    public Article nextCrawlDate(ZonedDateTime nextCrawlDate) {
        this.nextCrawlDate = nextCrawlDate;
        return this;
    }

    public void setNextCrawlDate(ZonedDateTime nextCrawlDate) {
        this.nextCrawlDate = nextCrawlDate;
    }

    public Integer getCrawlCount() {
        return crawlCount;
    }

    public Article crawlCount(Integer crawlCount) {
        this.crawlCount = crawlCount;
        return this;
    }

    public void setCrawlCount(Integer crawlCount) {
        this.crawlCount = crawlCount;
    }

    public SortedSet<Comment> getComments() {
        return comments;
    }

    public Article comments(SortedSet<Comment> comments) {
        this.comments = comments;
        return this;
    }

    public Article addComments(Comment comment) {
        this.comments.add(comment);
        comment.setArticle(this);
        return this;
    }

    public Article removeComments(Comment comment) {
        this.comments.remove(comment);
        comment.setArticle(null);
        return this;
    }

    public void setComments(SortedSet<Comment> comments) {
        this.comments = comments;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Article article = (Article) o;
        if (article.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), article.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Article{" +
            "id=" + getId() +
            ", valid='" + isValid() + "'" +
            ", publishedDate='" + getPublishedDate() + "'" +
            ", crawledDate='" + getCrawledDate() + "'" +
            ", articleId='" + getArticleId() + "'" +
            ", title='" + getTitle() + "'" +
            ", subTitle='" + getSubTitle() + "'" +
            ", imageUrl='" + getImageUrl() + "'" +
            ", content='" + getContent() + "'" +
            ", contentHtml='" + getContentHtml() + "'" +
            ", newspaper='" + getNewspaper() + "'" +
            ", url='" + getUrl() + "'" +
            ", nextCrawlDate='" + getNextCrawlDate() + "'" +
            ", crawlCount=" + getCrawlCount() +
            "}";
    }
}
