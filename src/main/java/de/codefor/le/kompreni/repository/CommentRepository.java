package de.codefor.le.kompreni.repository;

import de.codefor.le.kompreni.domain.Comment;
import de.codefor.le.kompreni.domain.Occurrence;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Comment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    @Query(value = "SELECT new de.codefor.le.kompreni.domain.Occurrence(author, count(author)) " +
                   "FROM Comment c " +
                   "GROUP BY author ")
    public List<Occurrence> findArticleCountByAuthor();

    public List<Comment> findAllByDeleted(Boolean deleted);

//    @Query(value = "SELECT * FROM USERS WHERE LASTNAME = ?1",
//        countQuery = "SELECT count(*) FROM USERS WHERE LASTNAME = ?1",
//        nativeQuery = true)
//    Page<User> findByLastname(String lastname, Pageable pageable);
//
//    @Query("select u from User u where u.lastname like ?1%")
//    List<User> findByAndSort(String lastname, Sort sort);
}
