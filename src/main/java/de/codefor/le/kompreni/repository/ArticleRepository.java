package de.codefor.le.kompreni.repository;

import de.codefor.le.kompreni.domain.Article;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.stream.Stream;


/**
 * Spring Data  repository for the Article entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {

    boolean existsByUrl(String url);

    Stream<Article> findByNextCrawlDateBefore(Date date);
}
