/*
   Copyright 2009 IBM Corp

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package de.codefor.le.kompreni.nlp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 
 * @author Jonathan Feinberg <jdf@us.ibm.com>
 * 
 */
public enum StopWords
{
    German();

	private final Set<String> stopwords = new HashSet<>();

	private StopWords() {

        try {

            this.stopwords.addAll(
                Files.lines(Paths.get(StopWords.class.getResource("/opennlp/german.stopwords").getPath()).toAbsolutePath())
                .collect(Collectors.toSet()));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
	}

	public boolean isStopWord(final String s)
	{
		if (s.length() == 1) {
			return true;
		}

		// check rightquotes as apostrophes
		return stopwords.contains(s.replace('\u2019', '\'').toLowerCase(Locale.GERMAN));
	}
}
