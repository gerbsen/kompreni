package de.codefor.le.kompreni.nlp;

import de.codefor.le.kompreni.domain.Comment;

import de.codefor.le.kompreni.domain.Occurrence;
import de.codefor.le.kompreni.repository.CommentRepository;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

@Service
public class CommentUtil {

    private static TokenizerME tokenizer = null;

    @Autowired
    private CommentRepository commentRepository;

    static {

        try {
            tokenizer = new TokenizerME(
                new TokenizerModel(CommentUtil.class.getResourceAsStream("/opennlp/de-token.bin")));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Occurrence> getGroupByAuthorCount(String order, Integer limit) {

        Comparator<Occurrence> occurrenceComparator = Comparator.comparingInt(Occurrence::getOccurence);
        if ( "DESC".equalsIgnoreCase(order) )
            occurrenceComparator = occurrenceComparator.reversed();

        return commentRepository.findArticleCountByAuthor()
                .stream()
                .sorted(occurrenceComparator)
                .limit(limit)
                .collect(Collectors.toList());
    }

    /**
     * This method returns the word frequencies for all comments contents.
     * It uses Apache OpenNLP to do the tokenizing and removes german stopwords.
     *
     * @param comments the comments to be counted
     * @return a list of pairs from token and occurrence
     */
    public static List<Occurrence> getWordFrequencies(List<Comment> comments, Integer limit, String order) {

        Comparator<Occurrence> occurrenceComparator = Comparator.comparingInt(Occurrence::getOccurence);
        if ( "DESC".equalsIgnoreCase(order) )
            occurrenceComparator = occurrenceComparator.reversed();

        // map the words to their count
        return comments
            .stream()
            .flatMap(comment -> (Arrays.asList(tokenizer.tokenize(comment.getOriginalContent()))).stream())
            .filter(s -> !StopWords.German.isStopWord(s))
            .collect(toMap(
                s -> s, // key is the word
                s -> 1, // value is 1
                Integer::sum))
                    .entrySet()
                    .stream()
                    .map(entry -> new Occurrence(entry))
                    .sorted(occurrenceComparator)
                    .limit(limit)
                    .collect(Collectors.toList());
    }
}
