package de.codefor.le.kompreni.crawl;

import de.codefor.le.kompreni.domain.Article;
import org.jsoup.HttpStatusException;

public class ArticleCrawlJob {

    private final Article article;

    public ArticleCrawlJob(Article article) {
        this.article = article;
    }

    public Article getArticle() {
        return this.article;
    }

    public boolean isLastArticle() {
        return this.getArticle() == null;
    }
}
