package de.codefor.le.kompreni.crawl;

import de.codefor.le.kompreni.domain.Comment;
import de.codefor.le.kompreni.repository.ArticleRepository;
import de.codefor.le.kompreni.repository.CommentRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

import java.util.*;
import java.util.concurrent.*;

@Component
public class LvzArticleCrawler {

    public static final String BASE_URL = "http://www.lvz.de";

    private static final Logger LOG = LoggerFactory.getLogger(LvzArticleCrawler.class);

    private final BlockingQueue<ArticleCrawlJob> queue = new ArrayBlockingQueue<>(1400);

    @Autowired
    private Environment environment;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private CommentRepository commentRepository;

    @PostConstruct
    public void init() {
        LOG.info(String.valueOf(Arrays.asList(environment.getDefaultProfiles())));
    }

    @Scheduled(fixedRate = 30 * 1000)
    public void printQueue() {

        if ( LOG.isInfoEnabled() )
            this.queue.stream().forEach(article -> System.out.println(article.getArticle().getUrl()));
    }

    @Scheduled(fixedRate = 30 * 60 * 1000)
    public void crawl() {

        ArticleCrawlJobProducer producer = new ArticleCrawlJobProducer(queue, articleRepository);
        LvzArticleCrawlJobConsumer consumer = new LvzArticleCrawlJobConsumer(queue, articleRepository, commentRepository);

        LOG.info("Starting to produce inspectLinks jobs");
        ExecutorService producerExecutorPool = Executors.newFixedThreadPool(1);
        producerExecutorPool.submit(producer);

        LOG.info("Starting to consume inspectLinks jobs");
        ExecutorService consumerExecutorPool = Executors.newFixedThreadPool(1);
        consumerExecutorPool.submit(consumer);

        ExecutorService recrawlCommentsExecutorPool = Executors.newFixedThreadPool(1);
        recrawlCommentsExecutorPool.submit(new LvzCommentUpdateJob());
    }

    public class LvzCommentDto {

        List<Comment> comments;
        boolean success;
        boolean displayLogin;

        public LvzCommentDto(){}
    }

    public static void main(String[] args) {

//        RestTemplate restTemplate = new RestTemplate();
//        List<Comment> comments = restTemplate.getForObject("http://www.lvz.de/comments/22932835", List.class);
//        LOG.info(comments.toString());

        String result = new RestTemplate().getForObject("http://www.lvz.de/comments/{articleId}", String.class, "22932835");
        JSONArray comments = new JSONObject(result).getJSONArray("comments");
        for ( int i = 0 ; i < comments.length() ; i++ ) {

            JSONObject jsonObject = comments.getJSONObject(i);
            Comment comment = new Comment();
            comment.setAuthor(jsonObject.getString("from"));
            comment.setContent(jsonObject.getString("content"));
            comment.setSubject(jsonObject.getString("subject"));
//            comment.setCreated(ZonedDateTime.now());

//            "updated": "2018-07-18T12:47:18+02:00",
//            "from": "Südvorstädter",
//            "is_moderator": false
        }

        LOG.info(comments.toString());


    }
}
