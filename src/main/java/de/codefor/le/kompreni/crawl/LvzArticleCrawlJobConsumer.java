package de.codefor.le.kompreni.crawl;

import de.codefor.le.kompreni.domain.Article;
import de.codefor.le.kompreni.domain.Comment;
import de.codefor.le.kompreni.repository.ArticleRepository;
import de.codefor.le.kompreni.repository.CommentRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;
import java.time.temporal.TemporalUnit;
import java.util.concurrent.BlockingQueue;

public class LvzArticleCrawlJobConsumer implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(LvzArticleCrawlJobConsumer.class);
    public static final String CONTENT_VIOLATION = "Aufgrund eines Verstoßes gegen die Nutzungsbedingungen wurde dieser Kommentar entfernt.";
    public static final String SUBJECT_VIOLATION = "Verstoß gegen Nutzungsrichtlinien";

    private final BlockingQueue<ArticleCrawlJob> queue;
    private final ArticleRepository articleRepository;
    private final CommentRepository commentRepository;

    public LvzArticleCrawlJobConsumer(BlockingQueue<ArticleCrawlJob> queue, ArticleRepository articleRepository, CommentRepository commentRepository) {
        this.queue = queue;
        this.articleRepository = articleRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    public void run() {

        while (true) {

            try {

                ArticleCrawlJob articleCrawlJob = this.queue.take();

                Document page = Jsoup.connect(articleCrawlJob.getArticle().getUrl()).get();

                // we need to check if this is an article or just a random page
                if ( isArticle(page, articleCrawlJob.getArticle()) ) {

                    int startIndex   = page.html().indexOf("\"pageArticleId\": \"");
                    int endIndex     = page.html().indexOf("\",", startIndex);
                    String articleId = page.html().substring(startIndex + 18, endIndex);

                    startIndex             = page.html().indexOf("\"pagePublishDate\": \"");
                    endIndex               = page.html().indexOf("\",", startIndex);
                    String pagePublishDate = page.html().substring(startIndex + 20, endIndex);

                    Article article = articleCrawlJob.getArticle();
                    article.setTitle(page.select(".pdb-article-teaser-breadcrumb-headline-title").get(0).text());
                    article.setSubTitle(page.select(".pdb-article-teaser-intro p").get(0).text());
                    article.setImageUrl(LvzArticleCrawler.BASE_URL +
                        page.select(".pdb-article-image-picture img").get(0).attr("src"));
                    article.setContent(page.select(".pdb-article-body").get(0).text());
                    article.setContentHtml(page.html());
                    article.setNewspaper("Leipziger Volkszeitung");
                    article.crawledDate(Instant.now());
                    article.setArticleId(articleId);
                    article.setPublishedDate(ZonedDateTime.parse(pagePublishDate).toInstant());
                    article.setNextCrawlDate(ZonedDateTime.now().plus(1, ChronoUnit.MINUTES));
                    article.setCrawlCount(1);

                    String result = new RestTemplate().getForObject("http://www.lvz.de/comments/{articleId}",
                        String.class, articleId);

                    if ( new JSONObject(result).has("comments") ) {

                        JSONArray comments = new JSONObject(result).getJSONArray("comments");
                        for ( int i = 0 ; i < comments.length() ; i++ ) {

                            JSONObject jsonObject = comments.getJSONObject(i);
                            Comment comment = new Comment();
                            comment.setIndex(i);
                            comment.setAuthor(jsonObject.getString("from"));
                            comment.setOriginalContent(jsonObject.getString("content"));
                            comment.setOriginalSubject(jsonObject.getString("subject"));
                            comment.setModerator(jsonObject.getBoolean("is_moderator"));
                            comment.setUpdated(ZonedDateTime.parse(jsonObject.getString("updated")).toInstant());
                            comment.setCreated(Instant.now());
                            comment.setDeleted(SUBJECT_VIOLATION.equals(comment.getOriginalSubject()) &&
                                CONTENT_VIOLATION.equals(comment.getOriginalContent()));
                            article.addComments(comment);
                        }

                        articleRepository.save(article);

                        if ( LOG.isInfoEnabled())
                            LOG.info(String.format("Successfully crawled '%s' and added to database.", article.getUrl()));
                    }
                }
                else {

                    if (LOG.isInfoEnabled())
                        LOG.debug(String.format("Not using '%s' for crawling, not an article.", articleCrawlJob.getArticle().getUrl()));
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isArticle(Document page, Article article) {

        return page.select(".pdb-article-teaser-breadcrumb-headline-title").size() > 0;
    }
}
