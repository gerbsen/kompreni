package de.codefor.le.kompreni.crawl;

import de.codefor.le.kompreni.domain.Article;
import de.codefor.le.kompreni.domain.Comment;
import de.codefor.le.kompreni.repository.ArticleRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.*;

import static de.codefor.le.kompreni.crawl.LvzArticleCrawlJobConsumer.CONTENT_VIOLATION;
import static de.codefor.le.kompreni.crawl.LvzArticleCrawlJobConsumer.SUBJECT_VIOLATION;

public class LvzCommentUpdateJob implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(LvzCommentUpdateJob.class);

    private ArticleRepository articleRepository;

    @Override
    public void run() {

        while (true) {

            articleRepository.findByNextCrawlDateBefore(new Date()).forEach(article -> {

                SortedSet<Comment> newComments = getNewComments(article.getArticleId());
                SortedSet<Comment> comments = article.getComments();

            });
        }
        
            
            
        
        
//        String result = new RestTemplate().getForObject("http://www.lvz.de/comments/{articleId}",
//            String.class, article.getArticleId());
//
//        if ( new JSONObject(result).has("comments") ) {
//
//            JSONArray comments = new JSONObject(result).getJSONArray("comments");
//            for ( int i = 0 ; i < comments.length() ; i++ ) {
//
//                JSONObject jsonObject = comments.getJSONObject(i);
//                Comment comment = new Comment();
//                comment.setAuthor(jsonObject.getString("from"));
//                comment.setOriginalContent(jsonObject.getString("content"));
//                comment.setOriginalSubject(jsonObject.getString("subject"));
//                comment.setModerator(jsonObject.getBoolean("is_moderator"));
//                comment.setUpdated(ZonedDateTime.parse(jsonObject.getString("updated")).toInstant());
//                comment.setCreated(Instant.now());
//                comment.setDeleted(SUBJECT_VIOLATION.equals(comment.getOriginalSubject()) &&
//                    CONTENT_VIOLATION.equals(comment.getOriginalContent()));
//                article.addComments(comment);
//            }
//
//            articleRepository.save(article);
//
//            if ( LOG.isInfoEnabled())
//                LOG.info(String.format("Successfully crawled '%s' and added to database.", article.getUrl()));
//        }
    }

    private SortedSet<Comment> getNewComments(String articleId) {
        
        return new TreeSet<>();
    }
}
