package de.codefor.le.kompreni.crawl;

import de.codefor.le.kompreni.domain.Article;
import de.codefor.le.kompreni.repository.ArticleRepository;
import de.codefor.le.kompreni.repository.CommentRepository;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

class ArticleCrawlJobProducer implements Runnable {

    public static final String BASE_URL = "http://www.lvz.de";
    private static final List<String> whitelist =
        Arrays.asList("/Leipzig/Stadtpolitik", "/Leipzig/Lokales",
                      "/Region/Mitteldeutschland", "/Nachrichten");
    private static final Logger LOG = LoggerFactory.getLogger(LvzArticleCrawler.class);
    private static final Integer MAX_DEPTH = 3;
    private final BlockingQueue<ArticleCrawlJob> queue;
    private final Set<String> database = new HashSet<>();
    private final ArticleRepository articleRepository;

    public ArticleCrawlJobProducer(BlockingQueue<ArticleCrawlJob> queue, ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
        this.queue = queue;
    }

    public void run() {

        try {

            if ( LOG.isInfoEnabled() )
                LOG.info("Starting article inspectLinks job producer.");

            ArticleCrawlJob crawlJob = inspectLinks("/", 0);

            if ( !crawlJob.isLastArticle() )
                queue.put(crawlJob);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public ArticleCrawlJob inspectLinks(String path, Integer depth) throws IOException, InterruptedException {

        if ( depth <= MAX_DEPTH ) {

            if ( LOG.isInfoEnabled())
                LOG.info(String.format("Inspecting '%s' for links.", BASE_URL + path));

            Document doc = Jsoup.connect(BASE_URL + path).get();

            // find all links of the start page
            for ( Element link : doc.select("a[href^='/']")) {

                String href = link.attr("href");

                if ( matchesWhiteList(whitelist, href) && // we only want certain parts of LVZ
                    // and we don't want to add pages that have already been crawled
                    !containedInDatabase(BASE_URL + href) ) {

                    Article article = new Article();
                    article.setUrl(BASE_URL + href);

                    // we might want to add it to the inspectLinks stack, but we don't want
                    // them in the database since we then have trash there
                    article.setValid(isValid(article));
                    articleRepository.save(article);

                    // #anchors or other meta links should not be crawled
                    if ( article.isValid() )
                        this.queue.offer(new ArticleCrawlJob(article));

                    // let's be gentle between crawls
                    TimeUnit.SECONDS.sleep(1);

                    if ( LOG.isInfoEnabled() )
                        LOG.info(String.format("Added '%s' to queue, depth: %s.", article.getUrl(), depth + 1));

                    inspectLinks(href, depth + 1);
                }
            }
        }

        return new ArticleCrawlJob(null);
    }

    /**
     * Non valid pages should only be used for crawling and no text analysis.
     *
     * @param article
     * @return
     */
    private boolean isValid(Article article) {
        return !article.getUrl().endsWith("#anchor") &&
            !"http://www.lvz.de/Leipzig/Stadtpolitik".equalsIgnoreCase(article.getUrl());
    }

    private boolean containedInDatabase(String href) {

        return articleRepository.existsByUrl(href);
    }

    private boolean matchesWhiteList(List<String> urlWhiteList, String url) {

        return urlWhiteList
            .stream()
            .filter(whitelistEntry -> url.toLowerCase().startsWith(whitelistEntry.toLowerCase()))
            .findAny()
            .isPresent();
    }
}
